<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shiping_id')->unsigned();
            $table->integer('payment_id')->unsigned();
            $table->text('description');
            $table->float('price');
            $table->string('address',100);
            $table->timestamps();
            $table->foreign('shiping_id')->references('id')->on('shiping');
            $table->foreign('payment_id')->references('id')->on('payment');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
